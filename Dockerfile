FROM python:3.11-slim-bookworm as builder

WORKDIR /app
COPY . .

ENV PYTHONUSERBASE=/app/__pypackages__
RUN pip install --user -r requirements.txt

FROM gcr.io/distroless/python3-debian12:debug

WORKDIR /app
COPY --from=builder /app .

ENV PYTHONUSERBASE=/app/__pypackages__
ENTRYPOINT ["sh", "-c", "python -uB -m gunicorn app:app -w 5 -b :${PORT:-8080}"]
