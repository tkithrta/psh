#!/usr/bin/env bash

git clone https://gitlab.com/tkithrta/psh.git

set -e

cd psh
docker build -t psh .

if [ ! -f .env.example ]; then
  curl -sLO https://gitlab.com/tkithrta/psh/-/raw/master/.env.example
fi
if [ ! -f data/config.json.example ]; then
  curl -sLo data/config.json.example --create-dirs https://gitlab.com/tkithrta/psh/-/raw/master/data/config.json.example
fi

if [ "$1" = 'https://example' ] || [ "$1" = 'https://www.example.com' ]; then
  echo 'This is an example.'
elif [ $# -gt 0 ]; then
  sed -e "s|https://example|$1|g" data/config.json.example > data/config.json
fi

echo "$(tr -dc a-zA-Z0-9 </dev/urandom 2>/dev/null | head -c48)" >> secret.txt
if [ -z "$GITPOD_WORKSPACE_ID" ]; then
  echo 'PYTHONWARNINGS=ignore' > .env
  echo 'CURL_CA_BUNDLE=localhost.crt' >> .env
  echo "SECRET=$(tail -1 secret.txt)" >> .env
else
  echo "SECRET=$(tail -1 secret.txt)" > .env
fi
if command -v openssl >/dev/null; then
  if [ -z "$GITPOD_WORKSPACE_ID" ]; then
    openssl genpkey -quiet -algorithm ec -pkeyopt ec_paramgen_curve:prime256v1 -out localhost.key
    openssl req -new -sha256 -subj /CN=localhost -key localhost.key -out localhost.csr
    openssl x509 -req -signkey localhost.key -in localhost.csr -out localhost.crt
  fi
  if [ ! -f id_rsa ]; then
    openssl genpkey -quiet -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -out id_rsa
  fi
  if [ ! -f id_rsa.pub ]; then
    openssl rsa -pubout -in id_rsa -out id_rsa.pub 2>/dev/null
  fi
elif command -v ssh-keygen >/dev/null; then
  if [ ! -f id_rsa ]; then
    ssh-keygen -q -b 4096 -m PKCS8 -t rsa -N '' -f id_rsa
  fi
  if [ ! -f id_rsa.pub ]; then
    ssh-keygen -e -m PKCS8 -f id_rsa > id_rsa.pub
  fi
fi
echo "PRIVATE_KEY=\"$(sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' id_rsa)\"" >> .env

docker run -d -v "$PWD/data:/app/data" -p "${PORT:-8080}:${PORT:-8080}" -e PORT --env-file=.env --name=psh psh
cd ..
