#!/usr/bin/env bash

set -e

if [ -n "$GITPOD_WORKSPACE_ID" ]; then
  pip install -r requirements-cgi.txt
  cd /etc/apache2/mods-enabled
  ln -s ../mods-available/cgi.load .
  cd - >/dev/null
  cp serve-cgi-bin.conf.gitpod.example /etc/apache2/conf-available/serve-cgi-bin.conf
fi
apachectl start
