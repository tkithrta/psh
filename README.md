# PSH

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tkithrta/psh)

[https://tkithrta.gitlab.io/u/psh](https://tkithrta.gitlab.io/u/psh)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/tkithrta/psh/-/raw/master/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
PSH: ActivityPub@Edge
```

```shell
$ curl -fsSL https://gitlab.com/tkithrta/psh/-/raw/master/setup.sh | bash -s -- https://www.example.com
$ docker run -d -v "$PWD/data:/app/data" -p 8080:8080 -e PORT=8080 --env-file=.env --name=psh registry.gitlab.com/tkithrta/psh/master
$ curl https://www.example.com/
PSH: ActivityPub@Edge
```

SPDX-License-Identifier: MIT
